/*
 *  SPDX-FileCopyrightText: 2016 Marco Martin <notmart@gmail.com>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick
import org.kde.kirigami
import "../../private"
import "../../templates" as T

T.SwipeListItem {
    id: listItem

    background: DefaultListItemBackground {}
}
